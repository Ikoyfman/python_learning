# Script Designed to calculate how many plates to use to get a balanced amount

#Bar Variable
bar = 45

def bar_weight():
    total = int(input("What weight are you trying to lift?"))
    totalnobar = total - bar
    return totalnobar

def bar_check(weight):
    while True:
        if weight <= 0 or weight == 0:
            print("You have an invalid weight.\n")
            decision = str.upper(input("Would you like to put in a new number? (Y/N) : "))
            if decision == "Y":
                weight = bar_weight()
            else:
                quit()
        else:
            break

def plate_calculator(weight):
    #plate variables
    p45 = 45
    p25  = 25
    p10 = 10
    p5 = 5
    p2half = float(2.5)

    
    print(f"Total weight without the bar is {weight} pounds")

    #45 Plate Computation
    x45 = weight / p45
    x45 = (int(x45 / 2)) * 2
    #remainder After 45 Plates
    remainder = weight - (x45 * p45)
    
    #25 Plate computation
    x25 = remainder / p25
    x25 = (int(x25 / 2)) * 2
    print(x25)


weight = bar_weight()
bar_check(weight)
plate_calculator(weight)