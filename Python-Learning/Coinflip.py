# File Designed to return a coin flip

# Import module
import random

def flip_coin():
    # generate a random number
    result = random.randint(0,1)
    if result is 0:
        result = "heads"
    else:
        result = "tails"

    return result
result = flip_coin()
print(result)