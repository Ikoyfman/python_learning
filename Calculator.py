# Script designed to be my first calculator

# Simple Function Addition
def func_addition(x, y):
    equals = x + y
    return equals

# Simple Function Subtraction
def func_subtraction(x, y):
    equals = x - y
    return equals

# Simple Function Multiplication
def func_multiplication(x, y):
    equals = x * y
    return equals

# Simple Function Division
def func_Division(x, y):
    equals = x / y
    return equals

# Switch for Operation
def switch_func():

    #User Input for the operation
    Choices = str(input("What Operation are you trying to perform?\n"))
    choicelist = ['+', '-', '/', '*']

    #Check if Operation is correct
    while Choices not in choicelist:
        Choices = input("Wrong Operations symbol. Please try again.7\n")
    else:
        operation = {
        '+': func_addition,
        '-': func_subtraction,
        '*': func_multiplication,
        '/': func_Division
        }.get(Choices)
        return operation

# User Input for two numbers and Operation
x = float(input("Enter your first number\n"))
y = float(input("Enter your second number\n"))
func = switch_func()

# Answers
total = str(func(x, y))
print("Your answer is equal to " + total)