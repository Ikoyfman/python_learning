# Script designed to be my first calculator

# Simple Function Addition
def func_addition(x, y):
    equals = x + y
    return equals

# Simple Function Subtraction
def func_subtraction(x, y):
    equals = x - y
    return equals

# Simple Function Multiplication
def func_multiplication(x, y):
    equals = x * y
    return equals

# Simple Function Division
def func_Division(x, y):
    equals = x / y
    return equals


Choices = raw_input("What Operation are you trying to perform?\n")
operation = Choices
operation = {
    '+': func_addition,
    '-': func_subtraction,
    '*': func_multiplication,
    '/': func_Division
}.get(Choices)
print(Choices)

