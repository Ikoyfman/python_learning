# Script sesigned to play rock paper scissors vs the computer

# Rock paper Scissors check

import random

# Weapon check


def rps_check(weapon):
    weapon = weapon.upper()
    while True:
        if weapon == "ROCK" or weapon == "PAPER" or weapon == "SCISSORS":
            return weapon
        else:
            weapon = input(
                "Not correct input, pleasse choose Rock, Paper, or Scissors")

# random choice


def random_rps():
    rps_list = ["Paper", "Rock", "Scissors"]
    randomchoice = random.choice(rps_list)
    randomchoice = randomchoice.upper()
    return randomchoice

# winner check


def rps_winner(p1, p2):

    # variables
    rock = "ROCK"
    paper = "PAPER"
    scissors = "SCISSORS"
    winner = "match is a tie"
    winner_p1 = "player 1 has won"
    winner_p2 = "player 2 has won"

    # Winner
    if p1 == p2:
        return winner
    elif p1 == rock:
        if p2 == paper:
            return winner_p2
        else:
            return winner_p1
    elif p1 == paper:
        if p2 == rock:
            return winner_p1
        else:
            return winner_p2
    else:
        if p2 == paper:
            return winner_p1
        else:
            return winner_p2


# human battle function
def rps_battle_human():
    print("HUMAN BATTLE! \n" * 5)
    print("" * 2)

    # human input choice
    weapon = input("player1 choose your weapon: (Rock, Paper, Scissors) \n")
    p1_weapon = rps_check(weapon)
    print("Player 1 has chosen " + p1_weapon)
    weapon2 = input("player2 choose your weapon: (Rock, Paper, Scissors) \n")
    p2_weapon = rps_check(weapon2)
    print("Player 2 has chosen " + p2_weapon)
    winner = rps_winner(p1_weapon, p2_weapon)
    print(winner)

# cpu battle function


def rps_battle_cpu():
    print("COMPUTER BATTLE! \n" * 5)
    print("" * 2)

    # human input choice
    weapon = input("player1 choose your weapon: (Rock, Paper, Scissors) \n")
    p1_weapon = rps_check(weapon)
    print("Player 1 has chosen " + p1_weapon)
    p2_weapon = random_rps()
    print("The Computer has chosen " + p2_weapon)
    winner = rps_winner(p1_weapon, p2_weapon)
    print(winner)


# Choose what kind of mode
print("Welcome to the game of Rock Paper Scissors \n")
print("please choose your game mode\n")

# choices variable
human = "human"
computer = "computer"
choice = input("Do you want to battle a human or a computer: ")

# while loop to check for answer validity
while True:
    if choice == human or choice == computer:
        if choice == human:
            rps_battle_human()
        else:
            rps_battle_cpu()
        break
    else:
        print("Not a valid selection! Please choose again")
        choice = input("Do you want to battle a human or a computer: ")