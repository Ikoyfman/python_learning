# My first python script

# Import OS for methods
import os

# Path to file directory
path = "C:\Windows\Temp"

# getting list of files
files = os.listdir(path)
print(files)
wait = input("PRESS ENTER TO CONTINUE.")
wait
# Looping files
for item in files:

    # getting item size
    item = path + "\\" + item
    itemsize = os.path.getsize(item)
    itemsize = (float(itemsize/1024/1024))
    
    # Deleting if item is too big
    if itemsize >= float(1.0):
        print(item + " is bigger then 1 Megabyte. Deleting Item")
        
        os.remove(item)
    else:
        print(item + " is smaller then 1 Megabyte. Leaving Item Alone")