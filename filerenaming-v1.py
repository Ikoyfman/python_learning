#Script designed to rename files

import  os

#path = r"C:\Users\ilyak\Desktop\Inna-Bien-Kristina"
path = r"C:\Users\ilyak\Desktop\Inna-Bien-Kristina\November2018\11-12-2018"

for root, dirs, files in os.walk(path):
    for file in files:
        if file.endswith(".mp3"):
            os.chdir(root)
            oldfilestr = list((file.split('_')[0]))
            newfilestr = (''.join(oldfilestr[0:4])) + "-" + (''.join(oldfilestr[4:8])) + "-"  + (''.join(oldfilestr[8:12]))+ ".mp3"
            if os.path.isfile(newfilestr) is False:
                os.rename(file,newfilestr)
            else:
                newfilestr = (''.join(oldfilestr[0:4])) + "-" + (''.join(oldfilestr[4:8])) + "-"  + (''.join(oldfilestr[8:12]))+ "extra" + ".mp3"
                os.rename(file, newfilestr)