import operator

fulltext = r"With this statement we told sorted to sort the numbers dict (its keys), and to sort them by using numbers' class method for retrieving values"

wordlist = fulltext.split()

worddict = {}
for word in wordlist:
    if word in worddict:
        # Increase
        worddict[word] += 1
    else:
        # add to dict
        worddict[word] = 1

    sortedwords = sorted(worddict.items(),)
print(sortedwords)