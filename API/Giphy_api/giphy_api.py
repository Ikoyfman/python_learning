import requests
import random

def get_cat_object():
    search = "cats"
    url = "https://api.giphy.com/v1/gifs/search"
    response = requests.get(
        url,
        headers={"Accept": "application/json"},
        params={
            "apikey": '7Q4qys4jX7eEh6TadCnJEYiGXN9XP4Y6',
            "q": search,
            "limit": 100
        }
    )
    datalinks = response.json()['data']
    datalink = datalinks[(random.randint(1,100))]['bitly_url']
    return datalink