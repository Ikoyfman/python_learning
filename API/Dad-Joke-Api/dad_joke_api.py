#
# Script designed to get back dad jokes
#

#dad joke subject
def dadjoke_subject():
    Term = input("Please type in your search query for a dad joke: ")
    return Term

#dad joke function
def Dad_joke(term):

    #Modules to Load for function
    import requests
    import random
    
    #URL and Headers
    def dadjoke_response(innerterm):
        url = "https://icanhazdadjoke.com/search"
        response = requests.get(
            url,
            headers={"Accept": "application/json"},
            params={"term": innerterm, "limit": 30}
        )
        data = response.json()
        return data
    
    #Data
    data = dadjoke_response(term)
    number_jokes = len(data["results"])
    while number_jokes == 0:
        decision = input(f"There are no jokes for this {term}, would you like to try a different search? (y/n)")
        if decision == "y":
            term = dadjoke_subject()
            data = dadjoke_response(term)
            number_jokes = len(data["results"])
        else:
            print("Exiting, have a wonderful day")
            break
    else:
        # Pick random joke
        random_index = random.randint(0, (number_jokes - 1))
        data = data["results"][random_index]["joke"]

        # Printing Results
        print(f"There were {number_jokes} jokes for the term {term}. Here is a random one\n")
        print(data)

Term = dadjoke_subject()
Dad_joke(Term)