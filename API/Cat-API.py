#
# Script to pull random cat facts
#


def get_cat_fact():
    import requests
    url = "https://catfact.ninja/facts"
    response = requests.get(
        url,
        headers={"Accept": "application/json"},
    )
    data = response.json()
    data = data['data'][0]["fact"]
    return data

x = 0
while x < 10:
    Catfact = get_cat_fact()
    x += 1
    print(Catfact)