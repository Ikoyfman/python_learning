import requests

CLIENT_ID = "d820abdefba8645"
CLIENT_SECRET = "b9a40f7aec77f451bdf4b867a6494001c708bc4c"

REDIRECT_URI = 'http://localhost:8080'
AUTHORIZE_URL = "https://api.imgur.com/oauth2/authorize"
ACCESS_TOKEN_URL = "https://api.imgur.com/oauth2/token"

Headers = {"client_id":CLIENT_ID,
"response_type":"token"}

# 1. Ask for an authorization code
token = requests.get(f'{AUTHORIZE_URL}?response_type=token&client_id={CLIENT_ID}',headers={"Accept": "application/json"})
authorize_token = token.headers['Set-Cookie']

# 2. The user logs in, accepts your client authentication request

# 3. Sketchfab redirects to your provided `redirect_uri` with the authorization code in the URL
# Ex : https://website.com/oauth2_redirect?code=123456789

# 4. Grab that code and exchange it for an `access_token`
info = requests.post(
    ACCESS_TOKEN_URL,
    data={
        'response_type': 'token',
        'client_id': CLIENT_ID,
        'client_secret': CLIENT_SECRET,
        'redirect_uri': REDIRECT_URI
    }
)
print(info)