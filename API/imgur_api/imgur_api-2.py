import requests,random

def get_catlink():
    #url and headers
    url = "https://api.imgur.com/3/gallery/t/cat"
    headers = {'Authorization': 'Client-ID d0c742220339f2c',
               "Accept": "application/json"}

    #Response list of cat images in dictionaries
    response = requests.get(url, headers=headers).json()["data"]["items"]
    number_cat_links = (len(response))
    choice = random.randint(1,number_cat_links)
    cat_choice = response[choice]['link']
    print(cat_choice)

get_catlink()